package com.demo.springboot.service.util;

import com.google.common.collect.Lists;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @ClassName MybatisGeneratorUtil
 * @Description mybatis 自动生成mapper、entity
 * @Author xiaoshami
 * @Date 2018/7/20 下午4:03
 **/
public class MybatisGeneratorUtil {

    public static void main(String[] args) {
        List<String> warnings = Lists.newArrayList();
        //如果这里出现空指针，直接写绝对路径即可。
        String genCfg = "/generator/generatorConfig.xml";
        // 获取文件
        File configFile = new File(MybatisGeneratorUtil.class.getResource(genCfg).getFile());
        // mybatis config 解析器
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = null;
        try {
            // 解析配置文件 获取配置信息
            config = cp.parseConfiguration(configFile);
        } catch (IOException | XMLParserException e) {
            e.printStackTrace();
        }
        // 是否覆盖 true: 是
        DefaultShellCallback callback = new DefaultShellCallback(true);
        MyBatisGenerator myBatisGenerator = null;
        try {
            myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
        try {
            myBatisGenerator.generate(null);
        } catch (SQLException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
