package com.demo.springboot.web.controller;

import com.demo.springboot.common.domain.result.ServerResultDO;
import com.demo.springboot.web.annotation.LocalLock;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * @ClassName HelloController
 * @Description 测试Controller
 * @Author xiaoshami
 * @Date 2018/7/9 下午1:45
 **/
@RestController
@Validated
public class TestController {


    @RequestMapping(value = "hello")
    @LocalLock
    public ServerResultDO<String> testHello(@RequestParam(required = false) @NotNull String token, @Range(min = 1, max = 2) String age) {

        ServerResultDO<String> resultDO = new ServerResultDO<>();
        String result =  "success - " + token;
        String age1 =  age;
        resultDO.setModule(result);
        resultDO.setSuccess(true);
        return resultDO;
    }

}
