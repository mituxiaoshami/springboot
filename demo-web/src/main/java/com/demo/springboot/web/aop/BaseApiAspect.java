package com.demo.springboot.web.aop;


import com.demo.springboot.common.domain.result.ServerResultDO;
import com.demo.springboot.common.enums.error.BaseErrorEnum;
import com.demo.springboot.web.exception.BaseException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.Enumeration;
import java.util.Objects;

/**
 * ClassName BaseApiAspect
 * Description 基础api切面
 * Author xiaoshami
 * Date 2018/7/9 上午11:22
 **/
@Component
@Aspect
public class BaseApiAspect {

    private static final Logger log = LoggerFactory.getLogger(BaseApiAspect.class);


    @Around("execution(com.demo.springboot.common.domain.result.ServerResultDO com.demo.springboot.web.controller..*.*(..))")
    public ServerResultDO doProcess(ProceedingJoinPoint point) {

        String targetClass = point.getTarget().getClass().getSimpleName();
        MethodSignature signature = (MethodSignature) point.getSignature();
        String methodName = signature.getMethod().getName();

        ServerResultDO result = new ServerResultDO();
        try {
            log.info("["+targetClass+"-"+methodName+"-start]");
            ServerResultDO serverResultDO = (ServerResultDO) point.proceed();
            log.info("["+targetClass+"-"+methodName+"-end]", targetClass, methodName);
            return serverResultDO;
        } catch (BaseException ex) { // 业务异常
            log.warn("[" + targetClass + "-" + methodName + "] BaseException,code=" + ex.getExceptionCode()
                     + ",message=" + ex.getMessage());
            return ServerResultDO.addError(ex.getExceptionCode(), ex.getMessage());
        } catch (ConstraintViolationException ex) { // 参数异常
            return ServerResultDO.addError(BaseErrorEnum.PARAM_ERROR.getCode(), ex.getMessage());
        } catch (Throwable e) { // 当系统发生不可预估的错误时 记录传进来的参数
            log.error("["+targetClass+"-"+methodName+"]error", e);
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (Objects.isNull(attributes)) {
                result.setSuccess(false);
                result.setErrorCode(BaseErrorEnum.SYSTEM_ERROR.getCode());
                result.setMessage(BaseErrorEnum.SYSTEM_ERROR.getMsg());
                return result;
            }
            HttpServletRequest request = attributes.getRequest();
            Enumeration<String> enu = request.getParameterNames();
            while (enu.hasMoreElements()) {
                String paraName = enu.nextElement();
                result.putContext(paraName, request.getParameter(paraName));
            }
            result.setSuccess(false);
            result.setErrorCode(BaseErrorEnum.SYSTEM_ERROR.getCode());
            result.setMessage(BaseErrorEnum.SYSTEM_ERROR.getMsg());
            return result;
        } finally {
           // 根据自己业务场景,做相应处理
        }

    }

}
