package com.demo.springboot.web.exception;

import com.demo.springboot.common.enums.BaseError;

/**
 * @ClassName BaseException
 * @Description 基础异常类
 * @Author xiaoshami
 * @Date 2018/7/9 上午11:42
 **/
public class BaseException extends RuntimeException{

    private static final long	serialVersionUID	= 1L;

    private String exceptionCode;

    public BaseException() {
        super();
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String code, String message) {
        super(message);
        this.exceptionCode = code;
    }

    public BaseException(BaseError error) {
        super(error.getMsg());
        this.exceptionCode = error.getCode();
    }

    public BaseException(String code, String message, Throwable cause) {
        super(message,cause);
        this.exceptionCode = code;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    /**
     * 这里的BaseException 只是作为流程上的判断作用，所以不需要使用堆栈信息，这里优化下，不再获取堆栈信息
     * @return
     */
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

}
