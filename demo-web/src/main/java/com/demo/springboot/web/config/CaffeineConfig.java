package com.demo.springboot.web.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.collect.Maps;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * ClassName CaffeineConfig
 * Description Caffeine本地缓存配置
 * Author xiaoshami
 * Date 2018/8/22 下午2:42
 **/
@Configuration
// 开启对缓存的支持
@EnableCaching
public class CaffeineConfig {


    private static final int DEFAULT_MAXSIZE = 50000;
    private static final int DEFAULT_TTL = 10;


    /**
     * 创建基于Caffeine的CacheManager
     * @return CacheManager
     */
    @Bean
    @Primary
    public CacheManager caffeineCacheManager() {

        CacheManager cacheManager = new CacheManager();

        Map<String, Cache> CACHES = Maps.newHashMap();
        // 存放到实例中
        for(Caches c : Caches.values()){
            CACHES.put(c.name(),Caffeine.newBuilder()
                                        .recordStats()
                                        .expireAfterWrite(c.getTtl(), TimeUnit.SECONDS)
                                        .maximumSize(c.getMaxSize()).build());
        }

        cacheManager.setCACHES(CACHES);

        return cacheManager;
    }



    /**
     * 定义cache名称、超时时长（秒）、最大容量
     * 每个cache缺省：10秒超时、最多缓存50000条数据，需要修改可以在构造方法的参数中指定。
     */
    public enum Caches{
        LOCAL_LOCK(5)
        ;

        Caches() {
        }

        Caches(int ttl) {
            this.ttl = ttl;
        }

        Caches(int ttl, int maxSize) {
            this.ttl = ttl;
            this.maxSize = maxSize;
        }

        private int maxSize = DEFAULT_MAXSIZE;	//最大數量
        private int ttl = DEFAULT_TTL;		//过期时间（秒）

        public int getMaxSize() {
            return maxSize;
        }
        public int getTtl() {
            return ttl;
        }
    }


    public class CacheManager extends CaffeineCacheManager {

        // key 缓存名称 value 缓存实例
        private Map<String, Cache> CACHES;

        public Map<String, Cache> getCACHES() {
            return CACHES;
        }

        private void setCACHES(Map<String, Cache> CACHES) {
            this.CACHES = CACHES;
        }
    }


}
