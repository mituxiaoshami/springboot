package com.demo.springboot.web;

import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.ConnectorStatistics;
import io.undertow.server.HandlerWrapper;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.GracefulShutdownHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServer;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@MapperScan("com.demo.springboot.mapper")
public class SpringbootApplication {

    private static final Logger log = LoggerFactory.getLogger(SpringbootApplication.class);

    // 一个项目中切记不要出现多个 main 函数，否在在打包的时候 spring-boot-maven-plugin 将找不到主函数（主动指定打包主函数入口除外...）
    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }


    /**
     * spring boot Undertow 优雅停机
     * Undertow 中会出现中断异常，那么就有可能对业务造成影响
     */
    @Component
    public class GracefulShutdown implements ApplicationListener<ContextClosedEvent> {

        @Autowired
        private GracefulShutdownWrapper gracefulShutdownWrapper;

        @Autowired
        private ServletWebServerApplicationContext context; // servlet容器上下文

        @Override
        public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {

            // 拒绝接受客户端发送过来的请求
            gracefulShutdownWrapper.getGracefulShutdownHandler().shutdown();
            try {
                UndertowServletWebServer webServer = (UndertowServletWebServer) context.getWebServer();
                Field field = webServer.getClass().getDeclaredField("undertow");
                field.setAccessible(true);
                // 获取servlet容器
                Undertow undertow = (Undertow) field.get(webServer);
                List<Undertow.ListenerInfo> listenerInfo = undertow.getListenerInfo();
                Undertow.ListenerInfo listener = listenerInfo.get(0);
                ConnectorStatistics connectorStatistics = listener.getConnectorStatistics();
                // 判断剩余的连接数是否大于0
                while (connectorStatistics.getActiveConnections() > 0) {

                }
            } catch (Exception e) {
                // Application Shutdown
            }
        }
    }


    @Component
    public class GracefulShutdownWrapper implements HandlerWrapper {

        private GracefulShutdownHandler gracefulShutdownHandler;

        @Override
        public HttpHandler wrap(HttpHandler httpHandler) {
            if (gracefulShutdownHandler == null) {
                this.gracefulShutdownHandler = new GracefulShutdownHandler(httpHandler);
            }
            return gracefulShutdownHandler;
        }

        public GracefulShutdownHandler getGracefulShutdownHandler() {
            return gracefulShutdownHandler;
        }

    }


    @Component
    public class UndertowExtraConfiguration {

        private final GracefulShutdownWrapper gracefulShutdownWrapper;

        @Bean
        public UndertowServletWebServerFactory servletWebServerFactory() {
            UndertowServletWebServerFactory factory = new UndertowServletWebServerFactory();
            factory.addDeploymentInfoCustomizers(deploymentInfo -> deploymentInfo.addOuterHandlerChainWrapper(gracefulShutdownWrapper));
            factory.addBuilderCustomizers(builder -> builder.setServerOption(UndertowOptions.ENABLE_STATISTICS, true));
            return factory;
        }

        public UndertowExtraConfiguration(GracefulShutdownWrapper gracefulShutdownWrapper) {
            this.gracefulShutdownWrapper = gracefulShutdownWrapper;
        }

    }


    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {

        return args -> {
            //log.info("Bean 打印开始");
            String[] beanNames = context.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            //Arrays.stream(beanNames).forEach(log::info);
        };

    }


}
