package com.demo.springboot.web.aop;


import com.demo.springboot.common.enums.error.BaseErrorEnum;
import com.demo.springboot.web.annotation.LocalLock;
import com.demo.springboot.web.config.CaffeineConfig.CacheManager;
import com.demo.springboot.web.config.CaffeineConfig.Caches;
import com.demo.springboot.web.exception.BaseException;
import com.github.benmanes.caffeine.cache.Cache;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Objects;
import java.util.UUID;

/**
 * ClassName LockAspect
 * Description 本地锁切面
 * Author xiaoshami
 * Date 2018/7/23 下午8:55
 **/
@Aspect
@Component
public class LockAspect {

    @Autowired
    private CacheManager cacheManager;

    /**
     * 打印日志log
     **/
    private static final Logger LOGGER = LoggerFactory.getLogger(LockAspect.class);

    /**
     * token前缀
     **/
    private static final String  DUPLICATE_TOKEN_KEY = "duplicate_token_key";

    /**
     * 本地缓存实例
     **/
    private static Cache<String, Object> CACHES;



    @Before("execution(public * *(..)) && @annotation(localLock)")
    public void before(final JoinPoint joinPoint, LocalLock localLock) {

        String targetClass = joinPoint.getTarget().getClass().getSimpleName();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodName = signature.getMethod().getName();

        // 如果注解对象不为空
        if (Objects.nonNull(localLock)) {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            // 如果为空，抛出系统异常
            if (Objects.isNull(attributes)) {
                throw new BaseException(BaseErrorEnum.SYSTEM_ERROR);
            }
            boolean isSaveSession=localLock.save();
            if (isSaveSession) {
                // 获取本地锁缓存实例
                CACHES = cacheManager.getCACHES().get(Caches.LOCAL_LOCK.name());
                String key = getDuplicateTokenKey(joinPoint);
                if (CACHES.getIfPresent(key) != null) {
                    throw new BaseException(BaseErrorEnum.RESUBMIT_ERROR);
                }
                // 如果是第一次请求,就将 key 当前对象压入缓存中
                CACHES.put(key, UUID.randomUUID().toString());
                LOGGER.info("["+targetClass+"-"+methodName+"] result=", true);
            }
        }
    }


    /**
     * 获取重复提交key
     * @param joinPoint 切点
     * @return token
     */
    private String getDuplicateTokenKey(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        return DUPLICATE_TOKEN_KEY + "," + "-" + methodName;
    }

}
