package com.demo.springboot.web.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 锁的注解 为了防止重复提交
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface LocalLock {

    /**保存重复提交标记 默认为需要保存*/
    boolean save() default true;

    /**
     * 过期时间 由于用的 本地缓存时 暂时这属性 集成 redis 需要用到(用分布式锁时,需要用到)
     * @return 过期时间
     */
    int expire() default 5;

}
