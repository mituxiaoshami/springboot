package com.demo.springboot.common.enums;

public interface BaseError {

    /**
     * 获取错误码code
     * @return
     */
    String getCode();

    /**
     * 获取错误信息
     * @return
     */
    String getMsg();

}
