package com.demo.springboot.common.domain.result;

import com.demo.springboot.common.constant.BaseConstants;
import com.demo.springboot.common.domain.BaseDO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName BaseResultDO
 * @Description 基础返回类
 * @Author xiaoshami
 * @Date 2018/7/6 下午3:44
 **/
public class BaseResultDO extends BaseDO {

    private static final long   serialVersionUID = 1L;

    private String  message = "";

    private boolean success = true;

    /**
     * 通讯码
     */
    private String              communicationCode;

    /**
     * 业务码:出错的时候用来放置错误码
     */
    private String              errorCode;

    /**
     * 步骤检查执行中的数据模型。
     * <p>
     * 在执行一些步骤检查完成之后，需要设置一下数据模型，以便调用步骤检查的业务对象可以获得数据
     * <p>
     * 值参考
     *
     */
    private Map<String, Object> context = new HashMap<String, Object>();


    public BaseResultDO() {
        this.setSuccess(success);
    }

    public BaseResultDO(boolean success) {
        this.setSuccess(success);
    }

    public static BaseResultDO addError(String code, String message) {
        BaseResultDO resultDO = new BaseResultDO(false);
        resultDO.setErrorCode(code);
        resultDO.setMessage(message);
        return resultDO;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /*
     * ———— set and get—————
     */

    public boolean isSuccess() {
        return StringUtils.equals(BaseConstants.COMMUNICATE_SUCCESS, this.communicationCode);
    }

    public boolean isFailure() {
        return !isSuccess();
    }

    public void setSuccess(boolean success) {
        if(success){
            this.setCommunicationCode(BaseConstants.COMMUNICATE_SUCCESS);
        }else{
            this.setCommunicationCode(BaseConstants.COMMUNICATE__ERROR);
        }
    }

    public String getMessage() {
        return message;
    }

    public BaseResultDO setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCommunicationCode() {
        return communicationCode;
    }


    public void setCommunicationCode(String communicationCode) {
        this.communicationCode = communicationCode;
    }


    /**
     * @param context
     *            the models to set
     */
    public void setContext(final Map<String, Object> context) {
        this.context = context;
    }

    public void putContext(final String key, final Object value) {
        this.context.put(key, value);
    }

    public void putContext(Map<String, Object> context) {
        if (context != null) {
            this.context.putAll(context);
        }
    }

    public Map<String, Object> getContext() {
        return context;
    }

}
