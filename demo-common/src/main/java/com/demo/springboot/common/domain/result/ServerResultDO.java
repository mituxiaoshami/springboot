package com.demo.springboot.common.domain.result;

import java.util.List;

/**
 * @ClassName ServerResultDO
 * @Description 服务返回值
 * @Author xiaoshami
 * @Date 2018/7/9 上午11:31
 **/
public class ServerResultDO<T> extends BaseResultDO {


    private static final long    serialVersionUID = 1L;
    private              T       module;
    private              List<T> modules;

    public ServerResultDO() {
        super();
    }

    public ServerResultDO(T module) {
        super();
        setSuccess(true);
        setModule(module);
    }

    public static ServerResultDO addError(String code, String message) {
        ServerResultDO resultDO = new ServerResultDO();
        resultDO.setSuccess(false);
        resultDO.setErrorCode(code);
        resultDO.setMessage(message);
        return resultDO;
    }

    public static <T> ServerResultDO<T> getResult() {
        return new ServerResultDO<T>();
    }

    public List<T> getModules() {
        return modules;
    }

    public void setModules(List<T> modules) {
        this.modules = modules;
    }

    public T getModule() {
        return module;
    }

    public void setModule(T module) {
        this.module = module;
    }
}
