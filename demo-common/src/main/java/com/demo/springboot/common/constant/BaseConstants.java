package com.demo.springboot.common.constant;

/**
 * @ClassName BaseConstants
 * @Description 基本常量类
 * @Author xiaoshami
 * @Date 2018/7/6 下午4:30
 **/
public class BaseConstants {


    /**
     * 通讯成功码
     */
    public static final String COMMUNICATE_SUCCESS = "200";

    /**
     * 通讯失败码-服务端运行异常（程序、环境、配置等导致异常，比如查询SQL语法等错误）返回 500
     */
    public static final String COMMUNICATE__ERROR = "500";


}
