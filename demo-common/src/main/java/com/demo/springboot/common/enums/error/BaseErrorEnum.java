package com.demo.springboot.common.enums.error;

import com.demo.springboot.common.constant.BaseConstants;
import com.demo.springboot.common.enums.BaseError;

/**
 * @ClassName BaseErrorEnum
 * @Description 基础错误枚举类
 * @Author xiaoshami
 * @Date 2018/7/9 上午11:06
 **/
public enum BaseErrorEnum implements BaseError {

    /**
     * 服务出错了
     **/
    SYSTEM_ERROR(BaseConstants.COMMUNICATE__ERROR, "服务出错了~"),


    /**
     * 请勿重复提交
     **/
    RESUBMIT_ERROR(BaseConstants.COMMUNICATE__ERROR, "请勿重复提交~"),



    /**
     * 参数校验失败
     **/
    PARAM_ERROR(BaseConstants.COMMUNICATE__ERROR, "参数校验失败");
    ;

    BaseErrorEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

}
