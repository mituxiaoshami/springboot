package com.demo.springboot.common.domain;

import com.demo.springboot.common.util.NoNullFieldStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @ClassName BaseDO
 * @Description 基础的DO,所有domain对象必须继承这个类
 * @Author xiaoshami
 * @Date 2018/7/6 下午3:11
 **/
public class BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, new NoNullFieldStringStyle());
    }

}
